import { Book } from "../entities";
import { BookDAO } from "./book-dao";
import { readFile, writeFile } from "fs/promises";
import { MissingResourceError } from "../errors";

export class BookDaoTextFile implements BookDAO
{
    async createBook(book: Book): Promise<Book> 
    {
        const fileData:Buffer = await readFile("C:/Users/thoma/OneDrive/Desktop/Revature/Week 2/day 1/libraryAPI/books.txt");
        const textData:string = fileData.toString(); // turn file into string.
        const books:Book[] = JSON.parse(textData); // turn JSON into object.
        book.bookId = Math.round(Math.random() * 1000); // random ID for the book.
        books.push(book);
        await writeFile("C:/Users/thoma/OneDrive/Desktop/Revature/Week 2/day 1/libraryAPI/books.txt", JSON.stringify(books));
        return book;
    }

    async getAllBooks():Promise<Book[]> 
    {
        const fileData:Buffer = await readFile("C:/Users/thoma/OneDrive/Desktop/Revature/Week 2/day 1/libraryAPI/books.txt");
        const textData:string = fileData.toString(); // turn file into string.
        const books:Book[] = JSON.parse(textData); // turn JSON into object.
        return books;
    }

    async getBookById(bookId: number):Promise<Book> 
    {
        const fileData:Buffer = await readFile("C:/Users/thoma/OneDrive/Desktop/Revature/Week 2/day 1/libraryAPI/books.txt");
        const textData:string = fileData.toString(); // turn file into string.
        const books:Book[] = JSON.parse(textData); // turn JSON into object.

        for (const book of books)
        {
            if (book.bookId === bookId)
            {
                return book;
            }
        }
        throw new MissingResourceError(`The book with id ${bookId} could not be located.`);
    }

    async updateBook(book: Book):Promise<Book> 
    {
        const fileData:Buffer = await readFile("C:/Users/thoma/OneDrive/Desktop/Revature/Week 2/day 1/libraryAPI/books.txt");
        const textData:string = fileData.toString(); // turn file into string.
        const books:Book[] = JSON.parse(textData); // turn JSON into object.
        
        for (let i = 0; i < books.length; i++)
        {
            if (books[i].bookId === book.bookId)
            {
                books[i] = book;
            }
        }

        await writeFile("C:/Users/thoma/OneDrive/Desktop/Revature/Week 2/day 1/libraryAPI/books.txt", JSON.stringify(books));
        return book;
    }

    async deleteBookById(bookId: number):Promise<boolean> 
    {
        const fileData:Buffer = await readFile("C:/Users/thoma/OneDrive/Desktop/Revature/Week 2/day 1/libraryAPI/books.txt");
        const textData:string = fileData.toString(); // turn file into string.
        const books:Book[] = JSON.parse(textData); // turn JSON into object.
        
        for (let i = 0; i < books.length; i++)
        {
            if (books[i].bookId === bookId)
            {
                books.splice(i); // remove that book.
                await writeFile("C:/Users/thoma/OneDrive/Desktop/Revature/Week 2/day 1/libraryAPI/books.txt", JSON.stringify(books));
                return true;
            }
        }
        
        return false;
    }
}

// This file is the Data layer