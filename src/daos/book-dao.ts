// DAO (Data Access Object)
// A class that is responsible for persisting an entity.
// A DAO should support the CRUD operations: Create, Read, Update, Delete

import {Book} from "../entities";

export interface BookDAO
{
    // These all return promises that contain "Books".

    // CREATE
    createBook(book:Book):Promise<Book>; // return a promise that will eventually be a book.

    // READ
    getAllBooks():Promise<Book[]>;
    getBookById(bookId:number):Promise<Book>;

    // UPDATE
    updateBook(book:Book):Promise<Book>;

    // DELETE
    deleteBookById(bookId:number):Promise<boolean>;
}

// This file is the Data layer