// An entity is a class that stores information that will ultimately be persisted somewhere.
// Usually very minimal logic.
// They should always have one field in them that is a unique indentifier (an ID).

export class Book
{
    constructor
    (
        public bookId:number, 
        public title:string, 
        public author:string,
        public isAvailable:boolean,
        public quality:number,
        public returnDate:number 
        // Typically dates are stored as unix epoch time, which is
        // seconds from midnight January 1970.
    )
    {
        //
    }
}