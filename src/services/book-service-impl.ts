import { BookDAO } from "../src/daos/book-dao";
import { BookDaoPostgres } from "../src/daos/book-dao-postgres";
import { BookDaoTextFile } from "../src/daos/book-dao-textfile-impl";
import { Book } from "../src/entities";
import BookService from "./book-service";

export class BookServiceImpl implements BookService
{
    bookDao:BookDAO = new BookDaoPostgres();

    registerBook(book: Book):Promise<Book> 
    {
        book.returnDate = 0;
        if (book.quality < 1)
        {
            book.quality = 1;
        }
        return this.bookDao.createBook(book);
    }

    retrieveAllBooks():Promise<Book[]> 
    {
        return this.bookDao.getAllBooks();
    }

    retrieveBookById(bookId: number):Promise<Book> 
    {
        return this.bookDao.getBookById(bookId);
    }

    // service methods also perform business logic.
    async checkoutBookById(bookId: number):Promise<Book> 
    {
        let book:Book = await this.bookDao.getBookById(bookId);
        book.isAvailable = false;
        book.returnDate = Date.now() + 1_209_600;// 2 weeks in seconds. can use underscores for readability.
        book = await this.bookDao.updateBook(book);
        return book;
    }

    checkInBookById(bookId: number):Promise<Book> 
    {
        throw new Error("Method not implemented.");
    }

    searchByTitle(title: string):Promise<Book[]> 
    {
        throw new Error("Method not implemented.");
    }

    modifyBook(book: Book):Promise<Book> 
    {
        throw new Error("Method not implemented.");
    }

    removeBookById(bookId: number):Promise<boolean> 
    {
        throw new Error("Method not implemented.");
    }
}

// This file is the service layer