import { Book } from "../src/entities";

/**
 * Your service interface should have all the methods that your RESTful webservice will
 * find helpful.
 * 
 * 1.) Methods that perform CRUD operations.
 * 2.) Business logic operations
 */
export default interface BookService
{
    registerBook(book:Book):Promise<Book>;
    retrieveAllBooks():Promise<Book[]>;
    retrieveBookById(bookId:number):Promise<Book>;
    checkoutBookById(bookId:number):Promise<Book>;
    checkInBookById(bookId:number):Promise<Book>;
    searchByTitle(title:string):Promise<Book[]>;
    modifyBook(book:Book):Promise<Book>;
    removeBookById(bookId:number):Promise<boolean>;
}

// This file is the service layer