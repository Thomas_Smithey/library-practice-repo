import express from "express";
import { ConsoleWriter } from "istanbul-lib-report";
import BookService from "../services/book-service";
import { BookServiceImpl } from "../services/book-service-impl";
import { Book } from "./entities";
import { MissingResourceError } from "./errors";

const app = express();
app.use(express.json()); // Middleware

// Our application routes should use services we create to do the heavy lifting.
// Try to minimize the amount of logic in your routes that is not related directly to HTTP requests and responses.

const bookService:BookService = new BookServiceImpl();

// Define routes
app.get("/books", async (req, res) =>
{
    const books:Book[] = await bookService.retrieveAllBooks();
    res.send(books);
});

app.get("/books/:id", async (req, res) =>
{
    try
    {
        const bookId:number = Number(req.params.id);
        const book:Book = await bookService.retrieveBookById(bookId);
        res.send(book);
    }
    catch(error)
    {
        if (error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
        }
    }
    finally
    {

    }
});

app.post("/books", async (req, res) =>
{
    let book:Book = req.body;
    book = await bookService.registerBook(book);
    res.send(book);
});

app.patch("/books/:id/checkout", async (req, res) =>
{
    const bookId = Number(req.params.id);
    const book = await bookService.checkoutBookById(bookId);
    res.send(book);
});

app.listen(3000, () => {console.log("Application started")});

// this file is the API layer