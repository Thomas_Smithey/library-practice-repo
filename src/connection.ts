import { Client } from "pg";

export const client = new Client
({
    user:"postgres",
    password:process.env.DBPASSWORD, // YOU SHOULD NEVER STORE PASSWORDS IN CODE.
    database:"librarydb",
    port:5432,
    host:"34.132.148.74"
});
client.connect();

// store passwords as environment variables then use:
// process.env.PASSWORD
// it is convention to capitalize the variable name.
// use jest ./src/connection.spec.ts