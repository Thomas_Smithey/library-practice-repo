import { client } from "../src/connection";

// client is the main object we will use to interact with our databases.

test("Should create a connection", async () =>
{
    const result = await client.query("select * from book");
    // need to await because we are fetching data stored on the DB in the cloud.
    // console.log(result); too much output
});

afterAll(()=>
{
    client.end();
});